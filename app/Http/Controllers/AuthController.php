<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('form');
    }

    public function welcome(Request $request) {
        $nama_dpn = $request["fname"];
        $nama_blkng = $request["lname"];
        $gabung = $nama_dpn." ".$nama_blkng;
        return view("welcomes", compact('gabung'));
    }
}
