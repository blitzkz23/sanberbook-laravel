<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sanberbook</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf <!-- Security untuk routing post harus ada-->
        <label for="fname">First Name:</label><br>
        <input type="text" id="fname" name="fname"><br>
        <label for="lname">Last Name:</label><br>
        <input type="text" name="lname" id="lname"><br>

        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="male">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="male">Other</label><br>

        <label for="nationality"><p>Nationality:</p></label>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysean">Malaysean</option>
            <option value="australean">Australean</option>
        </select>

        <br>

        <p>Languages Spoken:</p>
        <input type="checkbox" name="languages1" id="languages1" value="Indonesia">
        <label for="bahasaindonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" name="languages2" id="languages2" value="English">
        <label for="english">English</label><br>
        <input type="checkbox" name="languages3" id="languages3" value="Other">
        <label for="other">Other</label><br>

        <p>Bio:</p>
        <textarea rows="4" cols="50" name="comment" form="usrform"></textarea>

        <br>

        <input type="submit" value="Sign Up">

    </form>

</body>
</html>