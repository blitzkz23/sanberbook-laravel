@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Enter New Cast</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form role="form" action="/casts" method="POST">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', '') }}" placeholder="Enter Nama" required>
            @error('nama') <!-- validasi frontend via bootstrap -->
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror          
          </div>
          <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" id="umur" name="umur" value="{{ old('umur', '') }}" placeholder="Enter Umur" required>
            @error('umur') <!-- validasi frontend via bootstrap -->
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror          
          </div>
          <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio','') }}" placeholder="Insert Bio" required>
            @error('bio')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>
        <!-- /.card-body -->
  
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Create</button>
        </div>
      </form>
    </div>
  </div>
@endsection