<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@dashboard');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/table', function() {
    return view('adminlte.master');
});

Route::get('/casts/create', 'CastController@create');
Route::get('/casts', 'CastController@index');
Route::post('/casts', 'CastController@store');
Route::get('/casts/{id}', 'CastController@show');
Route::get('/casts/{id}/edit', 'CastController@edit');
Route::put('/casts/{id}', 'CastController@update');
Route::delete('/casts/{id}', 'CastController@destroy');


